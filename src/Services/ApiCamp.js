import axios from 'axios';
import ApiConfigCamp from '../Config/ApiConfigCamp'
const instance = axios.create({
  baseURL: ApiConfigCamp.baseUrl,
  headers: {
    'Content-Type': 'application/json',
  },
});
export const CampaignApi = {
  getCampaign: async (id) => {
    try {
      const res = await instance.get(`campany/${id}`);
      return res.data;
    } catch (e) {
      console.log(e);
    }
  },
  getCampaigns: async () => {
    try {
      const res = await instance.get(`campany`);
      return res.data;
    } catch (e) {
      console.log(e);
    }
  },
  updateCampaign: async (id, data) => {
    try {
      const res = await instance.put(`campany/${id}`, {
        ...data
      });
      return res.status;
    } catch (e) { 
      console.log(e);
    }
  },
  deleteCampaign: async (id) => {
    try {
      const res = await instance.delete(`campany/${id}`);
      return res;
    } catch (e) { 
      console.log(e);
    }
  },
  addCampaign: async (data) => {
    try {
      const res = await instance.post(`campany/`, {
        ...data
      });
      return res.data;
    } catch (e) { 
      console.log(e);
    }
  }
}
